<?php

/**
 * @file
 * Administrative page callbacks for the
 * google analytics event tracking UI module.
 */

/**
 * Implements hook_form().
 */
function google_analytics_event_tracking_ui_form($form) {
  $form['trackevent_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings for tracking of an event'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['trackevent_form']['event_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of event to be tracked'),
    '#size' => 50,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Name the event , that has to be tracked'),
  );

  $form['trackevent_form']['selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Mention the selector /css id of the event'),
    '#size' => 50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#description' => t('css selector id of the event'),
  );

  $form['trackevent_form']['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category of the event'),
    '#size' => 50,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#description' => t('The name you supply for the group of objects you want to track.'),
  );

  $form['trackevent_form']['action'] = array(
    '#type' => 'textfield',
    '#title' => t('Action on the event'),
    '#size' => 20,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Uniquely paired with each category, and commonly used to define the type of user interaction for the web object.'),
  );
  $form['trackevent_form']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label of the event'),
    '#size' => 20,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('An optional string to provide additional dimensions to the event data.'),
  );

  $form['trackevent_form']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value of the event'),
    '#size' => 20,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('An optional string to provide value.'),
  );
  $form['trackevent_form']['noninteraction'] = array(
    '#type' => 'radios',
    '#title' => t('Non Interaction'),
    '#maxlength' => 1,
    '#description' => t('A boolean that when set to true, indicates that the event hit will not be used in bounce-rate calculation.'),
    '#options' => array(t('TRUE'), t('FALSE')),
    '#required' => TRUE,
  );

  $form['trackevent_form']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and Continue'),
  );
  return $form;
}

/**
 * Implements hook_form().
 */
function google_analytics_event_tracking_ui_override_form($form) {
  $form['overrideevent'] = array(
    '#type' => 'fieldset',
    '#title' => t('Override Event Tracking'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['overrideevent']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Override'),
    '#submit' => array('google_analytics_event_tracking_ui_override'),
  );

  $form['overridestatus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Override Status'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['overridestatus']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Current Status'),
    '#submit' => array('google_analytics_event_tracking_ui_override_status'),
  );
  return $form;
}

/**
 * Implements hook_form().
 */
function google_analytics_event_tracking_ui_remove_form($form) {
  $form['removeevent_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings for removing of event'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['removeevent_form']['selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Mention the selector /css id of the event which you want to delete'),
    '#size' => 50,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('css selector id of the event'),
    '#autocomplete_path' => 'selector_id/autocomplete',
  );

  $form['removeevent_form']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and Continue'),
  );
  return $form;
}

/**
 * Function for search autocomplete for selector field.
 */
function google_analytics_event_tracking_ui_selector_id_autocomplete($string) {
  $matches = array();
  $return = variable_get('google_analytics_et_selectors');

  foreach ($return as $row) {
    $matches[$row["selector"]] = check_plain($row["selector"]);
  }
  drupal_json_output($matches);
}

/**
 * Function to toggle the override event tracking option.
 */
function google_analytics_event_tracking_ui_override($form, &$form_state) {
  google_analytics_et_override_event_tracking();
  $status = google_analytics_et_get_event_trackering_override_status();
  if ($status == TRUE) {
    $output = t('Google Analytics Event Tracking is Being Overwritten.');
    drupal_set_message(check_plain($output));
  }
  elseif ($status == FALSE) {
    $output = t('Google Analytics Event Tracking is Not Being Overritten.');
    drupal_set_message(check_plain($output));
  }
}

/**
 * Function to get the current status,whether event tracking is TRUE/FALSE.
 */
function google_analytics_event_tracking_ui_override_status($form, &$form_state) {
  $status = google_analytics_et_get_event_trackering_override_status();
  if ($status == 1) {
    $output = t('Current  Status : Google Analytics Event Tracking is Being Overwritten.');
    drupal_set_message(check_plain($output));
  }
  elseif ($status == 0) {
    $output = t('Current  Status : Google Analytics Event Tracking is Not Being Overritten.');
    drupal_set_message(check_plain($output));
  }
}

/**
 * Implements hook_form_submit().
 */
function google_analytics_event_tracking_ui_form_submit($form, &$form_state) {
  $selector_array = array(
    'event' => $form_state['values']['event_name'],
    'selector' => $form_state['values']['selector'],
    'category' => $form_state['values']['category'],
    'action' => $form_state['values']['action'],
    'label' => $form_state['values']['label'],
    'value' => $form_state['values']['value'],
    'noninteraction' => $form_state['values']['noninteraction'],
  );

  google_analytics_et_add_event_tracker($selector_array);
  drupal_set_message(t('Selector event added'));
}

/**
 * Implements hook_form_submit().
 */
function google_analytics_event_tracking_ui_remove_form_submit($form, &$form_state) {
  $selector = $form_state['values']['selector'];
  google_analytics_et_remove_event_tracker($selector);
  drupal_set_message(t('Selector event deleted'));
}
