Module: Google Analytics Event Tracking UI

Description
===========
Provides UI for the Google Analytics Event Tracking module which allows events 
in your site to be tracked by Google Analytics.

Requirements
============

* Google Analytics user account

Installation
============
* Copy the 'googleanalyticseventtrackingui' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
======
* Adding an event
  Fill in the fields for :- event , selector,category,action,label,value
  and noninteraction for the event which has to be tracked.
 
* Deleting an event
  Fill the selector id of the event.

*Overriding an event
  Override :- Toggle the override event tracking option . 
  This turns off the event tracking defined by all modules.
  Override Status :- To get the current status of event
  whether it is set to TRUE/FALSE.
 